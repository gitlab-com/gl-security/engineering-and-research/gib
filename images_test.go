package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
	gitlab "gitlab.com/gitlab-org/api/client-go"
	"sigs.k8s.io/yaml"

	log "github.com/sirupsen/logrus"
)

// TestNewDockerImagesRequestedHandler tests the constructor
func TestNewDockerImagesRequestedHandler(t *testing.T) {
	tests := []struct {
		name     string
		git      *GitLabClient
		daysBack int
		wantErr  bool
		errMsg   string
	}{
		{
			name:     "valid parameters",
			git:      &GitLabClient{},
			daysBack: 7,
			wantErr:  false,
		},
		{
			name:     "nil git client",
			git:      nil,
			daysBack: 7,
			wantErr:  true,
			errMsg:   "*GitLabClient can't be nil",
		},
		{
			name:     "invalid days back",
			git:      &GitLabClient{},
			daysBack: 0,
			wantErr:  true,
			errMsg:   "daysBack must be greater than 0",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handler, err := NewDockerImagesRequestedHandler(tt.git, tt.daysBack)

			if tt.wantErr {
				assert.Error(t, err)
				assert.Nil(t, handler)
				assert.Equal(t, tt.errMsg, err.Error())
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, handler)
				assert.Equal(t, tt.git, handler.gitLabClient)
				assert.Equal(t, tt.daysBack, handler.daysBack)
			}
		})
	}
}

func TestDockerImagesRequested(t *testing.T) {
	// Setup
	mux, ts := testServer(t)
	defer ts.Close()

	// Create jobs timestamps needed for the jobs struct
	job1CreatedAt := time.Now().AddDate(0, 0, -3)
	job2CreatedAt := job1CreatedAt.Add(time.Minute)
	job3CreatedAt := job2CreatedAt.Add(time.Minute)
	job4CreatedAt := job3CreatedAt.Add(time.Minute)

	firstCall := true

	mux.HandleFunc("/api/v4/projects/gitlab-org%2Fproject1/jobs", func(w http.ResponseWriter, r *http.Request) {
		var jobs []gitlab.Job

		if firstCall {
			// Initial run will find only one job
			jobs = []gitlab.Job{
				{ID: 1, Name: "job1", CreatedAt: &job1CreatedAt},
			}
		} else {
			// Second run will find 4 jobs
			jobs = []gitlab.Job{
				{ID: 4, Name: "job4", CreatedAt: &job4CreatedAt},
				{ID: 3, Name: "job3", CreatedAt: &job3CreatedAt},
				{ID: 2, Name: "job2", CreatedAt: &job2CreatedAt},
				{ID: 1, Name: "job1", CreatedAt: &job1CreatedAt},
			}
		}

		firstCall = false

		j, err := json.Marshal(jobs)
		if err != nil {
			t.Fatal(err)
		}

		fmt.Fprintln(w, string(j))
	}).Methods(http.MethodGet)

	mux.HandleFunc("/api/v4/projects/gitlab-org%2Fproject1/jobs/1/trace", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Using docker image sha256:somesha for image1:latest with digest image1@sha:digest1")
	}).Methods(http.MethodGet)

	// Create temp data-dir
	dataDir, err := os.MkdirTemp("", "datadir")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dataDir)

	project1Path := filepath.Join(dataDir, "gitlab-org", "project1")
	// Configure dataDir with a main `gitlab-org` group, and an existing project
	err = os.MkdirAll(project1Path, 0o700)
	if err != nil {
		t.Fatal(err)
	}

	// Store project in project1
	project1 := &Project{
		StoragePath: project1Path,
		Project: &gitlab.Project{
			PathWithNamespace: "gitlab-org/project1",
			Name:              "Project 1",
		},
	}

	err = project1.Save()
	if err != nil {
		t.Fatal(err)
	}

	os.Setenv("GITLAB_API_TOKEN", "asdf123")
	os.Setenv("GITLAB_API_BASEURL", ts.URL)
	os.Setenv("DATA_DIR_PATH", dataDir)

	log.SetFormatter(&log.TextFormatter{
		DisableColors:    true,
		DisableTimestamp: true,
	})

	output := new(bytes.Buffer)
	app := NewApp()
	app.Writer = output

	t.Run("Initial run", func(t *testing.T) {
		if err := app.Run([]string{os.Args[0], "images", "requested"}); err != nil {
			t.Fatal(err)
		}

		want := &DockerImageRequests{
			LastJobID: 1,
			Images: map[ImageName][]JobRequest{
				"image1:latest": {
					{
						JobName:    "job1",
						LastJobID:  1,
						LastUsedAt: job1CreatedAt,
						Digest:     "sha:digest1",
					},
				},
			},
		}

		got, err := LoadDockerImageRequestsFromFile(project1Path)
		if err != nil {
			t.Fatal(err)
		}

		if diff := cmp.Diff(want, got); diff != "" {
			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}
	})
	t.Run("Incremental run", func(t *testing.T) {
		// Running a second time, this time job 2 and 3 have a trace that will match the regexp:
		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fproject1/jobs/2/trace", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, "Using docker image sha256:somesha for image1:latest with digest image1@sha:digest2")
		}).Methods(http.MethodGet)

		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fproject1/jobs/3/trace", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, "Using docker image sha256:somesha for image3:latest with digest image1@sha:digest3")
		}).Methods(http.MethodGet)

		err = app.Run([]string{os.Args[0], "images", "requested"})
		if err != nil {
			t.Fatal(err)
		}

		want := &DockerImageRequests{
			LastJobID: 4,
			Images: map[ImageName][]JobRequest{
				"image1:latest": {
					{
						JobName:    "job1",
						LastJobID:  1,
						LastUsedAt: job1CreatedAt,
						Digest:     "sha:digest1",
					},
					{
						JobName:    "job2",
						LastJobID:  2,
						LastUsedAt: job2CreatedAt,
						Digest:     "sha:digest2",
					},
				},
				"image3:latest": {
					{
						JobName:    "job3",
						LastJobID:  3,
						LastUsedAt: job3CreatedAt,
						Digest:     "sha:digest3",
					},
				},
			},
		}

		got, err := LoadDockerImageRequestsFromFile(project1Path)
		if err != nil {
			t.Fatal(err)
		}
		if diff := cmp.Diff(want, got); diff != "" {
			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}
	})
}

func TestCleanupOldJobRequests(t *testing.T) {
	now := time.Now()

	tests := []struct {
		name     string
		input    *DockerImageRequests
		expected *DockerImageRequests
	}{
		{
			name: "removes old requests and keeps recent ones",
			input: &DockerImageRequests{
				LastJobID: 4,
				Images: map[ImageName][]JobRequest{
					"image1:latest": {
						{
							JobName:    "old-job",
							LastJobID:  1,
							LastUsedAt: now.AddDate(0, -4, 0), // 4 months old
							Digest:     "sha:old",
						},
						{
							JobName:    "recent-job",
							LastJobID:  2,
							LastUsedAt: now.AddDate(0, -2, 0), // 2 months old
							Digest:     "sha:recent",
						},
					},
					"image2:latest": {
						{
							JobName:    "old-job2",
							LastJobID:  3,
							LastUsedAt: now.AddDate(0, -5, 0), // 5 months old
							Digest:     "sha:old2",
						},
					},
				},
			},
			expected: &DockerImageRequests{
				LastJobID: 4,
				Images: map[ImageName][]JobRequest{
					"image1:latest": {
						{
							JobName:    "recent-job",
							LastJobID:  2,
							LastUsedAt: now.AddDate(0, -2, 0),
							Digest:     "sha:recent",
						},
					},
				},
			},
		},
		{
			name: "keeps all recent requests",
			input: &DockerImageRequests{
				LastJobID: 2,
				Images: map[ImageName][]JobRequest{
					"image1:latest": {
						{
							JobName:    "recent-job1",
							LastJobID:  1,
							LastUsedAt: now.AddDate(0, -1, 0), // 1 month old
							Digest:     "sha:recent1",
						},
						{
							JobName:    "recent-job2",
							LastJobID:  2,
							LastUsedAt: now.AddDate(0, -2, 0), // 2 months old
							Digest:     "sha:recent2",
						},
					},
				},
			},
			expected: &DockerImageRequests{
				LastJobID: 2,
				Images: map[ImageName][]JobRequest{
					"image1:latest": {
						{
							JobName:    "recent-job1",
							LastJobID:  1,
							LastUsedAt: now.AddDate(0, -1, 0),
							Digest:     "sha:recent1",
						},
						{
							JobName:    "recent-job2",
							LastJobID:  2,
							LastUsedAt: now.AddDate(0, -2, 0),
							Digest:     "sha:recent2",
						},
					},
				},
			},
		},
		{
			name: "removes all old requests",
			input: &DockerImageRequests{
				LastJobID: 2,
				Images: map[ImageName][]JobRequest{
					"image1:latest": {
						{
							JobName:    "old-job1",
							LastJobID:  1,
							LastUsedAt: now.AddDate(0, -4, 0), // 4 months old
							Digest:     "sha:old1",
						},
					},
					"image2:latest": {
						{
							JobName:    "old-job2",
							LastJobID:  2,
							LastUsedAt: now.AddDate(0, -5, 0), // 5 months old
							Digest:     "sha:old2",
						},
					},
				},
			},
			expected: &DockerImageRequests{
				LastJobID: 2,
				Images:    map[ImageName][]JobRequest{},
			},
		},
		{
			name: "handles empty requests",
			input: &DockerImageRequests{
				LastJobID: 0,
				Images:    map[ImageName][]JobRequest{},
			},
			expected: &DockerImageRequests{
				LastJobID: 0,
				Images:    map[ImageName][]JobRequest{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cleanupOldJobRequests(tt.input)

			if diff := cmp.Diff(tt.expected, tt.input); diff != "" {
				t.Errorf("cleanupOldJobRequests() mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func TestLoadDockerImageRequestsFromFile_WithCleanup(t *testing.T) {
	// Create temp directory
	tempDir, err := os.MkdirTemp("", "docker-requests-test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tempDir)

	now := time.Now()

	// Create test data
	testData := &DockerImageRequests{
		LastJobID: 2,
		Images: map[ImageName][]JobRequest{
			"image1:latest": {
				{
					JobName:    "old-job",
					LastJobID:  1,
					LastUsedAt: now.AddDate(0, -4, 0), // 4 months old
					Digest:     "sha:old",
				},
				{
					JobName:    "recent-job",
					LastJobID:  2,
					LastUsedAt: now.AddDate(0, -1, 0), // 1 month old
					Digest:     "sha:recent",
				},
			},
		},
	}

	// Write test data to file
	testFile := filepath.Join(tempDir, DockerImageRequestsFile)
	yamlData, err := yaml.Marshal(testData)
	if err != nil {
		t.Fatal(err)
	}
	if err := os.WriteFile(testFile, yamlData, 0o644); err != nil {
		t.Fatal(err)
	}

	// Test loading and cleanup
	got, err := LoadDockerImageRequestsFromFile(tempDir)
	if err != nil {
		t.Fatal(err)
	}

	expected := &DockerImageRequests{
		LastJobID: 2,
		Images: map[ImageName][]JobRequest{
			"image1:latest": {
				{
					JobName:    "recent-job",
					LastJobID:  2,
					LastUsedAt: now.AddDate(0, -1, 0),
					Digest:     "sha:recent",
				},
			},
		},
	}

	if diff := cmp.Diff(expected, got); diff != "" {
		t.Errorf("LoadDockerImageRequestsFromFile() mismatch (-want +got):\n%s", diff)
	}
}
