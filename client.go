package main

import (
	"fmt"
	"net/url"
	"path"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

// GitLabClient is a gitlab API client (go-gitlab) created from the cli context
type GitLabClient struct {
	*gitlab.Client
	*GraphQLClient
	DisableAllDownloads              bool
	DisableDependenciesDownload      bool
	DisableVulnerabilitiesDownload   bool
	DisableCIConfigDownload          bool
	DisableProtectedBranchesDownload bool
	DisableApprovalRulesDownload     bool
	DisableCICDSettingsDownload      bool
	DisableJobTokenInboundAllowList  bool
}

func newClient(c *cli.Context) *GitLabClient {
	// The graphQLClient is a bit of a hack to leave all the auth stuff to the go-gitlab library
	// even if the GraphQL API isn't supported by go-gitlab
	var git *gitlab.Client
	var graphQLClient *GraphQLClient
	var err error
	gitlabURL := c.String(apiBaseURLFlagName)

	graphQLURL, err := url.Parse(gitlabURL)
	if err != nil {
		log.Fatalf("failed to create client: %v", err)
	}

	graphQLURL.Path = path.Join(graphQLURL.Path, "api/graphql")
	// We don't support other authentication methods than Access Tokens for GraphQL
	graphQLClient = NewGraphQLClient(c.String(apiTokenFlagName), graphQLURL.String())

	// Workaround until https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues/25372 is fixed
	// TLDR; Rate limiting headers are not always present, leading the program to fail because of 429 http errors
	clientOpts := []gitlab.ClientOptionFunc{
		gitlab.WithBaseURL(gitlabURL),
		gitlab.WithCustomRetryWaitMinMax(100*time.Millisecond, time.Minute),
		gitlab.WithCustomRetryMax(100),
	}

	switch {
	case c.String(apiTokenFlagName) != "":
		git, err = gitlab.NewClient(c.String(apiTokenFlagName), clientOpts...)
		if err != nil {
			log.Fatalf("failed to create client: %v", err)
		}
	case c.String("username") != "" && c.String("password") != "":
		git, err = gitlab.NewBasicAuthClient(c.String(usernameFlagName), c.String(passwordFlagName), clientOpts...)
		if err != nil {
			log.Fatalf("failed to create client: %v", err)
		}
	default:
		git, err = gitlab.NewClient("", clientOpts...)
		if err != nil {
			log.Fatalf("failed to create client: %v", err)
		}
	}
	return &GitLabClient{
		Client:                           git,
		GraphQLClient:                    graphQLClient,
		DisableAllDownloads:              c.Bool("disable-all-dl"),
		DisableDependenciesDownload:      c.Bool("disable-deps-dl"),
		DisableVulnerabilitiesDownload:   c.Bool("disable-vulns-dl"),
		DisableCIConfigDownload:          c.Bool("disable-ciconfig-dl"),
		DisableProtectedBranchesDownload: c.Bool("disable-protected-branches-dl"),
		DisableApprovalRulesDownload:     c.Bool("disable-approvals-rules-dl"),
		DisableCICDSettingsDownload:      c.Bool("disable-cicd-settings-dl") || graphQLClient == nil,
		DisableJobTokenInboundAllowList:  c.Bool("disable-job-token-inbound-allowlist-dl"),
	}
}

func newClientFrom(git *GitLabClient, token string) (*GitLabClient, error) {
	newGit, err := gitlab.NewClient(token, gitlab.WithBaseURL(git.BaseURL().String()))
	if err != nil {
		return git, fmt.Errorf("failed to create client: %s", err)
	}

	newGQL := NewGraphQLClient(token, git.GraphQLClient.url)

	return &GitLabClient{
		Client:                           newGit,
		GraphQLClient:                    newGQL,
		DisableAllDownloads:              git.DisableAllDownloads,
		DisableDependenciesDownload:      git.DisableDependenciesDownload,
		DisableVulnerabilitiesDownload:   git.DisableVulnerabilitiesDownload,
		DisableCIConfigDownload:          git.DisableCIConfigDownload,
		DisableProtectedBranchesDownload: git.DisableProtectedBranchesDownload,
		DisableApprovalRulesDownload:     git.DisableApprovalRulesDownload,
		DisableJobTokenInboundAllowList:  git.DisableJobTokenInboundAllowList,
	}, nil
}
