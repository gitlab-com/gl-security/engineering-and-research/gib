package main

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"regexp"

	log "github.com/sirupsen/logrus"
	gitlab "gitlab.com/gitlab-org/api/client-go"
	"sigs.k8s.io/yaml"

	"github.com/urfave/cli/v2"
)

func validateCmd(c *cli.Context) error {
	dir := c.Args().First()
	dataDir := c.String(dataDirFlagName)
	var numDirs int

	if dir == "" {
		dir = dataDir
	}
	err := filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		// Validate regular files
		if !d.IsDir() {
			err := validateFile(path, d, c.Bool("quick"))
			if err != nil {
				log.WithField("path", path).Error("Invalid file")
				return err
			}
			return nil
		}

		if path != dataDir {
			numDirs++
		}

		return nil
	})
	if err != nil {
		return err
	}

	logger := log.WithField("dataDir", dataDir)
	if dir != dataDir {
		logger = logger.WithField("path", dir)
	}
	logger.Infof("Data directory clean (%d directories scanned)", numDirs)
	return nil
}

// FileValidator defines a function type for validating file content
type FileValidator func([]byte) error

// validators maps filenames to their validation functions
var validators = map[string]FileValidator{
	DependenciesFile:           validateJSON[[]Dependency],
	GroupMetadataFile:          validateJSON[gitlab.Group],
	ProjectMetadataFile:        validateJSON[Project],
	ShortProjectMetadataFile:   validateJSON[ShortProject],
	PropertiesFile:             validateProperties,
	VulnerabilitiesFile:        validateJSON[[]Vulnerability],
	CIConfigFile:               validateJSON[gitlab.ProjectLintResult],
	ViolationsFile:             validateJSON[OPAViolations],
	ProtectedBranchesFile:      validateJSON[[]gitlab.ProtectedBranch],
	ApprovalConfigFile:         validateJSON[gitlab.ProjectApprovals],
	ApprovalRulesFile:          validateJSON[[]gitlab.ProjectApprovalRule],
	CICDSettingsFile:           validateJSON[CICDSettings],
	DockerImageRequestsFile:    validateJSON[DockerImageRequests],
	JobTokenScopeAllowlistFile: validateJSON[[]gitlab.Project],
}

// knownFiles is a set of files that don't need validation
var knownFiles = map[string]struct{}{
	SyncTokenFile: {},
	IgnoreFile:    {},
	"README.md":   {},
	".gitkeep":    {},
	".DS_Store":   {},
}

// validateJSON is a generic function to validate JSON unmarshaling
func validateJSON[T any](data []byte) error {
	return json.Unmarshal(data, new(T))
}

// validateProperties handles the special case of properties file validation
func validateProperties(data []byte) error {
	var p Properties
	if err := yaml.Unmarshal(data, &p); err != nil {
		return err
	}

	for _, d := range p.Dependencies {
		if d.VersionExtract != nil {
			if _, err := regexp.Compile(d.VersionExtract.Regexp); err != nil {
				return fmt.Errorf("Dependency %q: %v", d.Name, err)
			}
		}
	}
	return nil
}

func validateFile(path string, d fs.DirEntry, quick bool) error {
	// Quick validation for non-properties files
	if quick && d.Name() != PropertiesFile {
		return nil
	}

	// Read file content
	b, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	log.WithField("file", path).Debug("Validating file")

	// Check if file is in known files list
	if _, ok := knownFiles[d.Name()]; ok {
		return nil
	}

	// Get validator function
	validator, ok := validators[d.Name()]
	if !ok {
		return fmt.Errorf("unexpected filename %q", d.Name())
	}

	// Run validation
	return validator(b)
}
