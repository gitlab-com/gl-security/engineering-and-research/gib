package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"regexp"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	gitlab "gitlab.com/gitlab-org/api/client-go"
	"sigs.k8s.io/yaml"
)

var imageRegExp = regexp.MustCompile(`(?m)^.*(.\[0K)?Using docker image (\S*) for (?P<requested>\S*) with digest (\S*@)(?P<digest>\S*)`)

// JobRequest holds data for a specific image used in a project
type JobRequest struct {
	// Job which pulled the image
	JobName string `json:"job_name,omitempty"`
	// Used to easily retrieve the last job which pulled the image
	LastJobID int `json:"last_job_id,omitempty"`
	// Used to clean-up requests files
	LastUsedAt time.Time `json:"last_used_at,omitempty"`
	// Digest of the image used in the job
	Digest string `json:"digest,omitempty"`
}

// ImageName is the name of the Image as a string
type ImageName string

// DockerImageRequests holds all images used in a project
type DockerImageRequests struct {
	// Map of images with metadata
	Images map[ImageName][]JobRequest `json:"images,omitempty"`
	// Checkpoint to stop downloading jobs when reached
	LastJobID int `json:"last_job_id,omitempty"`
}

func getDockerImagesRequestedCmd(c *cli.Context) error {
	git := newClient(c)

	// Walk the data tree
	dataDir := filepath.Clean(c.String(dataDirFlagName))
	dir := c.Args().First()
	if dir == "" {
		dir = dataDir
	}

	w, err := NewDockerImagesRequestedHandler(git, c.Int("days-back"))
	if err != nil {
		return err
	}

	err = walkDataDir(dir, dataDir, w)
	if err != nil {
		return fmt.Errorf("error walking the path %q: %v", dir, err)
	}
	return nil
}

// DockerImagesRequestedHandler is a DataDir walker to download consumed images
type DockerImagesRequestedHandler struct {
	gitLabClient *GitLabClient
	// Continue to fetch jobs until daysBack
	daysBack int
}

// NewDockerImagesRequestedHandler is a DockerImagesRequestedHandler constructor
func NewDockerImagesRequestedHandler(git *GitLabClient, daysBack int) (*DockerImagesRequestedHandler, error) {
	if git == nil {
		return nil, fmt.Errorf("*GitLabClient can't be nil")
	}
	if daysBack < 1 {
		return nil, fmt.Errorf("daysBack must be greater than 0")
	}
	return &DockerImagesRequestedHandler{gitLabClient: git, daysBack: daysBack}, nil
}

// GitLabClient returns the GitLabClient of the handler
func (h DockerImagesRequestedHandler) GitLabClient() *GitLabClient {
	return h.gitLabClient
}

// jobProcessor process jobs to extract Docker images requested from their trace (raw job log)
type jobProcessor struct {
	project  *Project
	git      *GitLabClient
	requests *DockerImageRequests
	mu       sync.Mutex
	wg       sync.WaitGroup
}

// newJobProcessor creates a new *JobProcessor
func newJobProcessor(project *Project, git *GitLabClient, requests *DockerImageRequests) *jobProcessor {
	return &jobProcessor{
		project:  project,
		git:      git,
		requests: requests,
	}
}

func (p *jobProcessor) processJob(job *gitlab.Job, errChan chan<- error) {
	defer p.wg.Done()

	trace, _, err := p.git.Jobs.GetTraceFile(p.project.PathWithNamespace, job.ID)
	if err != nil {
		return
	}

	match, err := readUntilMatch(trace, imageRegExp)
	if err != nil {
		errChan <- err
		return
	}

	if len(match) == 0 {
		p.project.logger().WithFields(log.Fields{"job_id": job.ID, "job_name": job.Name}).Warn("Pattern not found in job output")
		return
	}

	p.processMatch(job, match)
}

func (p *jobProcessor) processMatch(job *gitlab.Job, match []string) {
	requested := ImageName(match[imageRegExp.SubexpIndex("requested")])
	digest := match[imageRegExp.SubexpIndex("digest")]
	request := JobRequest{
		JobName:    job.Name,
		LastUsedAt: *job.CreatedAt,
		LastJobID:  job.ID,
		Digest:     digest,
	}

	p.mu.Lock()
	defer p.mu.Unlock()

	if p.updateExistingRequest(requested, request) {
		return
	}
	p.addNewRequest(requested, request, job)
}

func (p *jobProcessor) updateExistingRequest(requested ImageName, request JobRequest) bool {
	for i, existingRequest := range p.requests.Images[requested] {
		if existingRequest.JobName == request.JobName && existingRequest.Digest == request.Digest {
			if request.LastJobID > existingRequest.LastJobID {
				p.requests.Images[requested][i].LastJobID = request.LastJobID
				p.requests.Images[requested][i].LastUsedAt = request.LastUsedAt
			}
			return true
		}
	}
	return false
}

func (p *jobProcessor) addNewRequest(requested ImageName, request JobRequest, job *gitlab.Job) {
	var foundMessage string
	if _, ok := p.requests.Images[requested]; ok {
		foundMessage = "Found new job+digest"
	} else {
		foundMessage = "Found new image"
	}

	log.WithFields(log.Fields{
		"job_id":         job.ID,
		"job_name":       request.JobName,
		"job_created_at": job.CreatedAt,
		"image":          requested,
		"digest":         request.Digest,
	}).Debug(foundMessage)

	p.requests.Images[requested] = append(p.requests.Images[requested], request)
}

func (p *jobProcessor) updateLastJobID(jobID int) {
	p.mu.Lock()
	if jobID > p.requests.LastJobID {
		p.requests.LastJobID = jobID
	}
	p.mu.Unlock()
}

// HandleSingleProject downloads images used by a single project
func (h DockerImagesRequestedHandler) HandleSingleProject(dataDir string, project *Project, git *GitLabClient) error {
	if project.BuildsAccessLevel == gitlab.DisabledAccessControl {
		project.logger().Info("Skipping project with disabled builds")
		return nil
	}

	files, err := os.ReadDir(project.StoragePath)
	if err != nil {
		return err
	}

	if hasIgnoreFile(files) {
		project.logger().Info("ignore file found, not fetching images")
		return nil
	}

	requests, err := LoadDockerImageRequestsFromFile(project.StoragePath)
	if err != nil {
		return err
	}

	project.logger().Info("Fetching docker images used in jobs")

	processor := newJobProcessor(project, git, requests)
	err = h.processProjectJobs(processor)
	if err != nil {
		return err
	}

	project.DockerImagesRequested = requests
	return project.StoreDockerImagesRequested()
}

func (h DockerImagesRequestedHandler) processProjectJobs(processor *jobProcessor) error {
	opts := processProjectJobsOptions()
	options := []gitlab.RequestOptionFunc{}

	lastJobID := processor.requests.LastJobID
	numberOfImages := len(processor.requests.Images)
	numberOfJobs := 0
	errChan := make(chan error, 100)

main_loop:
	for {
		jobs, response, err := processor.git.Jobs.ListProjectJobs(processor.project.PathWithNamespace, &opts, options...)
		if err != nil {
			processor.project.logger().Errorf("Failed to get jobs (URL: %s)", response.Request.URL)
			return err
		}

		for _, job := range jobs {
			processor.updateLastJobID(job.ID)

			if job.ID <= lastJobID {
				processor.project.logger().Debugf("Stop searching for new images, reached last job ID (%d)", processor.requests.LastJobID)
				break main_loop
			}

			if job.CreatedAt.Before(time.Now().AddDate(0, 0, -h.daysBack)) {
				break main_loop
			}

			numberOfJobs++
			processor.wg.Add(1)
			go processor.processJob(job, errChan)
		}

		// Exit the loop when we've seen all pages.
		if response.NextLink == "" {
			break
		}

		// Prepare options for the next query with the next page
		options = []gitlab.RequestOptionFunc{
			gitlab.WithKeysetPaginationParameters(response.NextLink),
		}
	}

	processor.wg.Wait()

	select {
	case err := <-errChan:
		return err
	default:
		processor.project.logger().Infof("Storing %d new docker images used (%d total) after parsing %d jobs",
			len(processor.requests.Images)-numberOfImages, len(processor.requests.Images), numberOfJobs)
		return nil
	}
}

func processProjectJobsOptions() gitlab.ListJobsOptions {
	return gitlab.ListJobsOptions{
		ListOptions: gitlab.ListOptions{
			Pagination: "keyset",
			PerPage:    100,
			OrderBy:    "id",
			Sort:       "desc",
		},
		Scope: &[]gitlab.BuildStateValue{gitlab.Success, gitlab.Failed},
	}
}

func readUntilMatch(reader io.Reader, re *regexp.Regexp) ([]string, error) {
	bufreader := bufio.NewReader(reader)

	var match []string

	// Read line by line
	for {
		var buffer bytes.Buffer
		line, isPrefix, err := bufreader.ReadLine()
		if err == io.EOF {
			break
		}
		if err != nil {
			return match, err
		}

		buffer.Write(line)
		if !isPrefix {
			buffer.WriteByte('\n')
		}

		match = re.FindStringSubmatch(buffer.String())
		if len(match) > 1 {
			break
		}
	}

	return match, nil
}

// HandleProjects downloads all images used by all projects under `path` in the `dataDir` provided
func (h DockerImagesRequestedHandler) HandleProjects(dataDir, path string, git *GitLabClient) error {
	// Get files from path to check if it's a group
	files, err := os.ReadDir(path)
	if err != nil {
		return err
	}

	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	fileInfo, err := file.Stat()
	if err != nil {
		return err
	}

	if isGroup(fs.FileInfoToDirEntry(fileInfo), files) {
		// Nothing to do for groups
		return nil
	}

	project, err := LoadProject(dataDir, path)
	if err != nil {
		return err
	}
	return h.HandleSingleProject(dataDir, project, git)
}

// LoadDockerImageRequestsFromFile loads requests from a previously saved file stored in the dataDir
func LoadDockerImageRequestsFromFile(projectStoragePath string) (*DockerImageRequests, error) {
	requests := &DockerImageRequests{Images: map[ImageName][]JobRequest{}}

	requestsFile := filepath.Join(projectStoragePath, DockerImageRequestsFile)
	if _, err := os.Stat(requestsFile); os.IsNotExist(err) {
		// The file might not be present, so we return a skeleton requests struct
		log.Debugf("Docker Image Requests file %q not found", requestsFile)
		return requests, nil
	}
	b, err := os.ReadFile(requestsFile)
	if err != nil {
		return nil, err
	}
	if string(b) == "" {
		return nil, fmt.Errorf("%s file is empty", requestsFile)
	}

	err = yaml.Unmarshal(b, &requests)
	if err != nil {
		return nil, err
	}

	// Clean up old job requests
	cleanupOldJobRequests(requests)

	return requests, nil
}

// cleanupOldJobRequests removes JobRequests older than 3 months from the given DockerImageRequests
func cleanupOldJobRequests(requests *DockerImageRequests) {
	threeMonthsAgo := time.Now().AddDate(0, -3, 0)

	// Create a new map to store cleaned up image requests
	cleanedImages := make(map[ImageName][]JobRequest)

	// Iterate through all images and their requests
	for imageName, jobRequests := range requests.Images {
		// Filter out requests older than 3 months
		var validRequests []JobRequest
		for _, request := range jobRequests {
			if request.LastUsedAt.After(threeMonthsAgo) {
				validRequests = append(validRequests, request)
			}
		}

		// Only keep the image if it has valid requests
		if len(validRequests) > 0 {
			cleanedImages[imageName] = validRequests
		}
	}

	// Replace the original Images map with the cleaned one
	requests.Images = cleanedImages
}
