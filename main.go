package main

import (
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

func main() {
	app := NewApp()
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

const (
	apiTokenFlagName   = "api-token"
	usernameFlagName   = "username"
	passwordFlagName   = "password"
	apiBaseURLFlagName = "api-baseurl"
	dataDirFlagName    = "data-dir"
)

var gitlabAPIFlags = []cli.Flag{
	&cli.StringFlag{
		Name:    usernameFlagName,
		Aliases: []string{"u"},
		Usage:   "Username for Basic Auth",
		EnvVars: []string{"GITLAB_USERNAME"},
	},
	&cli.StringFlag{
		Name:    passwordFlagName,
		Aliases: []string{"p"},
		Usage:   "Password for Basic Auth",
		EnvVars: []string{"GITLAB_PASSWORD"},
	},
	&cli.StringFlag{
		Name:    apiTokenFlagName,
		Aliases: []string{"t"},
		Usage:   "Personal/Project Access Token",
		EnvVars: []string{"GITLAB_API_TOKEN"},
	},
	&cli.StringFlag{
		Name:    apiBaseURLFlagName,
		Value:   "https://gitlab.com/",
		Usage:   "Base URL of the GitLab instance",
		EnvVars: []string{"GITLAB_API_BASEURL"},
	},
}

// NewApp returns a *cli.App with all commands and flags configured
func NewApp() *cli.App {
	app := cli.NewApp()
	app.Authors = []*cli.Author{{Name: "GitLab"}}
	app.Compiled = time.Now()
	app.EnableBashCompletion = true
	app.HelpName = "gib"
	app.Usage = "GitLab Inventory Builder"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    "log-level",
			Aliases: []string{"l"},
			Usage:   "panic|fatal|error|warn|info|debug|trace",
			Value:   "info",
			EnvVars: []string{"LOGLEVEL"},
		},
		&cli.BoolFlag{
			Name:    "color",
			Aliases: []string{"c"},
			Usage:   "Force color output (useful when using `op run`)",
			EnvVars: []string{"FORCE_COLOR"},
		},
		&cli.StringFlag{
			Name:      dataDirFlagName,
			Usage:     "Folder to store projects data",
			Value:     "./data",
			EnvVars:   []string{"DATA_DIR_PATH"},
			TakesFile: true,
		},
	}

	app.Before = func(c *cli.Context) error {
		level, err := log.ParseLevel(c.String("log-level"))
		if err != nil {
			return err
		}
		log.SetLevel(level)
		log.SetOutput(c.App.Writer)
		if c.Bool("color") {
			log.SetFormatter(&log.TextFormatter{
				ForceColors: true,
			})
		}
		return nil
	}

	app.Commands = []*cli.Command{
		{
			Name:    "compliance",
			Aliases: []string{"c"},
			Usage:   "Compliance (policies)",
			Subcommands: []*cli.Command{
				{
					Name:      "run",
					Usage:     "Run Compliance (OPA) script",
					ArgsUsage: "[subdirectory of DATA_DIR_PATH]",
					Action:    runCompliance,
				},
				{
					Name:   "export",
					Usage:  "Export violation from the SQLite DB",
					Action: exportViolationsCmd,
				},
				{
					Name:  "create-issues",
					Usage: "Create issues for each violation (idempotent)",
					Flags: append(gitlabAPIFlags, []cli.Flag{
						&cli.StringFlag{
							Name:    "project-id",
							Usage:   "Project ID",
							EnvVars: []string{"PROJECT_ID", "CI_PROJECT_ID"},
						},
						&cli.StringFlag{
							Name:    "violations-file",
							Usage:   "Violations (see the export command) file path",
							Value:   filepath.Join("tmp", "violations.json"),
							EnvVars: []string{"VIOLATIONS_FILE"},
						},
					}...),
					Action: createIssuesForViolationsCmd,
				},
			},
		},
		{
			Name:    "generate-reports",
			Aliases: []string{"g"},
			Usage:   "Generate static HTML reports",
			Action:  generateReportsCmd,
		},
		{
			Name:      "sync",
			Aliases:   []string{"s"},
			Usage:     "Sync local data using the GitLab API",
			ArgsUsage: "[subdirectory of DATA_DIR_PATH]",
			Flags: append(gitlabAPIFlags, []cli.Flag{
				&cli.BoolFlag{
					Name:    "disable-all-dl",
					Usage:   "Disable all downloads at once",
					EnvVars: []string{"DISABLE_ALL_DOWNLOADS"},
				},
				&cli.BoolFlag{
					Name:    "disable-deps-dl",
					Usage:   "Disable dependencies download",
					EnvVars: []string{"DISABLE_DEPENDENCIES_DOWNLOAD"},
				},
				&cli.BoolFlag{
					Name:    "disable-vulns-dl",
					Usage:   "Disable vulnerabilities download",
					EnvVars: []string{"DISABLE_VULNERABILITIES_DOWNLOAD"},
				},
				&cli.BoolFlag{
					Name:    "disable-ciconfig-dl",
					Usage:   "Disable CI Config download",
					EnvVars: []string{"DISABLE_CICONFIG_DOWNLOAD"},
				},
				&cli.BoolFlag{
					Name:    "disable-protected-branches-dl",
					Usage:   "Disable Protected Branches download",
					EnvVars: []string{"DISABLE_PROTECTED_BRANCHES_DOWNLOAD"},
				},
				&cli.BoolFlag{
					Name:    "disable-approvals-rules-dl",
					Usage:   "Disable Approval Rules download",
					EnvVars: []string{"DISABLE_APPROVAL_RULES_DOWNLOAD"},
				},
				&cli.BoolFlag{
					Name:    "disable-cicd-settings-dl",
					Usage:   "Disable CI/CD Settings download",
					EnvVars: []string{"DISABLE_CICD_SETTINGS_DOWNLOAD"},
				},
				&cli.BoolFlag{
					Name:    "disable-job-token-inbound-allowlist-dl",
					Usage:   "Disable Job Token Inbound Allow List download",
					EnvVars: []string{"DISABLE_JOBTOKENINBOUNDALLOWLIST_DOWNLOAD"},
				},
			}...),
			Action: syncCmd,
		},
		{
			Name:    "images",
			Usage:   "Inventory docker images in projects",
			Aliases: []string{"i"},
			Subcommands: []*cli.Command{
				{
					Name:      "requested",
					Aliases:   []string{"r"},
					Usage:     "Fetch docker images used in projects",
					ArgsUsage: "[subdirectory of DATA_DIR_PATH]",
					Flags: append(gitlabAPIFlags, []cli.Flag{
						&cli.IntFlag{
							Name:    "days-back",
							Usage:   "Number of days in the past to lookup for jobs",
							EnvVars: []string{"DAYS_BACK"},
							Value:   7,
						},
					}...),
					Action: getDockerImagesRequestedCmd,
				},
			},
		},
		{
			Name:    "update-db",
			Aliases: []string{"u"},
			Usage:   "Generate or update a local sqlite3 DB reflecting the data folder",
			Action:  updateDBCmd,
		},
		{
			Name:      "validate",
			Aliases:   []string{"v"},
			Usage:     "Validate local data",
			ArgsUsage: "[subdirectory of DATA_DIR_PATH]",
			Flags: []cli.Flag{
				&cli.BoolFlag{
					Name:    "quick",
					Aliases: []string{"q"},
					Value:   false,
					Usage:   "Quick validation (only properties.yml files)",
					EnvVars: []string{"QUICK_VALIDATION"},
				},
			},
			Action: validateCmd,
		},
	}

	return app
}
