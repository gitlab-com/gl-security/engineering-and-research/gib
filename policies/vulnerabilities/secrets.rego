# GitLab Personal Access Tokens have a specific prefix (`glpat-`)
# Which makes them easily searchable. Generaly, these vulnerabilities are very
# good candidate for true positives and must be triaged ASAP.
#
# This rule reports detected vulnerabilities for this kind of tokens.
#
# See https://docs.gitlab.com/ee/user/application_security/secret_detection/
# and https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

package gitlab.vulnerabilities

import rego.v1

detected_glpat contains vulnerability if {
	vulnerability := input[_]
	vulnerability.state == "detected"
	vulnerability.description = "GitLab Personal Access Token"
}

violation contains {"description": description, "key": "detected_glpat", "msg": msg} if {
	vulnerability := detected_glpat[_]
	msg := sprintf("Untriaged GLPAT secret (id: %d)", [vulnerability.id])
	description := sprintf("Untriaged Secret Detection findings related to a GitLab Personal Access Token (id: %d)", [vulnerability.id])
}
