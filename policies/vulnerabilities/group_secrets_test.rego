package gitlab.vulnerabilities

import rego.v1

test_project_with_group_secrets if {
	vulnerabilities := [
		{
			"id": 1,
			"state": "detected",
			"description": "",
			"title": "Variable TESTVARIABLENAME_PROTECTED was unexpected and appears to be set at the group level",
			"scanner": {
				"id": "semgrep",
				"name": "Semgrep",
			},
			"identifiers": [{
				"type": "semgrep_type",
				"name": "Semgrep - group_ci_cd_variables",
				"value": "group_ci_cd_variables",
			}],
		},
		{
			"id": 2,
			"state": "confirmed",
			"description": "",
			"title": "Variable SUPERSECRET was unexpected and appears to be set at the group level",
			"scanner": {
				"id": "semgrep",
				"name": "Semgrep",
			},
			"identifiers": [{
				"type": "semgrep_type",
				"name": "Semgrep - group_ci_cd_variables",
				"value": "group_ci_cd_variables",
			}],
		},
	]
	result := violation with input as vulnerabilities
	result == {
		{
			"msg": "Untriaged Group CI/CD secret (id: 1)",
			"description": "Untriaged Group CI/CD vulnerability: Variable TESTVARIABLENAME_PROTECTED was unexpected and appears to be set at the group level",
			"key": "detected_group_secret",
		},
		{
			"msg": "Untriaged Group CI/CD secret (id: 2)",
			"description": "Untriaged Group CI/CD vulnerability: Variable SUPERSECRET was unexpected and appears to be set at the group level",
			"key": "detected_group_secret",
		},
	}
}

test_project_without_group_secrets if {
	vulnerabilities := [
		{
			"id": 1,
			"state": "dismissed",
			"description": "",
			"title": "Variable TESTVARIABLENAME_PROTECTED was unexpected and appears to be set at the group level",
			"scanner": {
				"id": "semgrep",
				"name": "Semgrep",
			},
			"identifiers": [{
				"type": "semgrep_type",
				"name": "Semgrep - group_ci_cd_variables",
				"value": "group_ci_cd_variables",
			}],
		},
		{
			"id": 2,
			"state": "resolved",
			"description": "",
			"title": "Variable SUPERSECRET was unexpected and appears to be set at the group level",
			"scanner": {
				"id": "semgrep",
				"name": "Semgrep",
			},
			"identifiers": [{
				"type": "semgrep_type",
				"name": "Semgrep - group_ci_cd_variables",
				"value": "group_ci_cd_variables",
			}],
		},
	]
	result := violation with input as vulnerabilities
	count(result) == 0
}
