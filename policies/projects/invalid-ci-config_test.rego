package gitlab.projects.invalid_ci_configs

import rego.v1

test_report_invalid_ci_config if {
	results := violation with input as {
		"project": {"empty_repo": false, "repository_access_level": "enabled"},
		"ci-config": {"valid": false, "errors": ["Included file `Security/License-Scanning.gitlab-ci.yml` is empty or does not exist!"], "warnings": [], "merged_yaml": ""},
	}
	results == {{
		"msg": "Invalid CI/CD configuration",
		"description": "Project CI/CD configuration is invalid. This means some jobs we might expect to run are not running. Errors: [\"Included file `Security/License-Scanning.gitlab-ci.yml` is empty or does not exist!\"]",
		"key": "invalid_ci_config",
	}}
}

test_report_invalid_ci_config_when_errors_is_null if {
	results := violation with input as {
		"project": {"empty_repo": false, "repository_access_level": "enabled"},
		"ci-config": {"valid": false, "errors": null, "warnings": null, "merged_yaml": ""},
	}
	results == {{
		"msg": "Invalid CI/CD configuration",
		"description": "Project CI/CD configuration is invalid. This means some jobs we might expect to run are not running. Errors: null",
		"key": "invalid_ci_config",
	}}
}

test_dont_report_valid_ci_config if {
	results := violation with input as {
		"project": {"empty_repo": false, "repository_access_level": "enabled"},
		"ci-config": {"valid": true, "errors": [], "warnings": [], "merged_yaml": ""},
	}

	results == set()
}

test_dont_report_for_inactive_repos if {
	results := violation with input as {
		"project": {"empty_repo": true, "repository_access_level": "disabled"},
		"ci-config": {"valid": false, "errors": [], "warnings": [], "merged_yaml": ""},
	}
	results == set()
}

test_dont_report_when_no_gitlab_ci_file if {
	results := violation with input as {
		"project": {"empty_repo": false, "repository_access_level": "enabled"},
		"ci-config": {"valid": false, "errors": ["Please provide content of .gitlab-ci.yml"], "warnings": [], "merged_yaml": ""},
	}
	results == set()
}
