package gitlab.projects.visibility

import rego.v1

import input.categories
import input.project

violation contains {"description": "Private project must remain private", "key": "private_project_exposed", "msg": "Project can't be public"} if {
	categories[_] == "keep_private"
	project.visibility == "public"
}

violation contains {"description": "Internal visibility is forbidden", "key": "internal_visibility", "msg": "Internal visibility"} if {
	project.visibility == "internal"
}
