package gitlab.projects.approvals

import rego.v1

import data.gitlab.projects.lib
import input.approvals
import input.project
import input.protected_branches

violation contains {"description": "Author should not be able to approve own MR", "key": "author_can_approve", "msg": "Merge Request author can self-approve"} if {
	lib.critical_project
	approvals.merge_requests_author_approval == true # Author should not be able to approve own MR, see #95
}

violation contains {"description": "Users should not be able to edit approval rules on individual MRs", "key": "users_can_edit_approval_rules", "msg": "Users can edit MR Approval Rules"} if {
	lib.critical_project
	approvals.disable_overriding_approvers_per_merge_request == false # Users should not be able to edit MR approval rules, see #97
}

violation contains {"description": "Committers should not be able to approve an MR after adding a commit", "key": "committers_can_approve", "msg": "Merge Request committers can self-approve"} if {
	lib.critical_project
	approvals.merge_requests_disable_committers_approval == false # Committers should not be able to approve MR after committing, see #96
}

# Approvals should be removed when new commits are added, see #98
violation contains {"description": "When new commits are added, approvals should be removed", "key": "approvals_not_removed", "msg": "Approvals are not removed when new commits are added"} if {
	lib.critical_project
	approvals.reset_approvals_on_push == false
	selective_code_owner_removals_false_or_not_available
}

# Account for instances when selective_code_owner_removals is not returned via the API
selective_code_owner_removals_false_or_not_available if {
	not approvals.selective_code_owner_removals
}

selective_code_owner_removals_false_or_not_available if {
	approvals.selective_code_owner_removals == false
}

# Approvals should be removed when new commits are added, see #98
violation contains {"description": "When new commits are added to CODEOWNER controlled files, approvals should be removed", "key": "codeowner_approvals_not_removed", "msg": "CODEOWNER approvals are not removed when new commits to their files are added"} if {
	lib.critical_project
	approvals.reset_approvals_on_push == false
	approvals.selective_code_owner_removals == true
	protected_branches[i].name == project.default_branch
	protected_branches[i].code_owner_approval_required == false
}
