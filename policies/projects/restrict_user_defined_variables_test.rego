package gitlab.projects.restrict_user_defined_variables

import rego.v1

test_dont_report_restrict_user_defined_variables_enabled if {
	results := violation with input as {"project": {"empty_repo": false, "repository_access_level": "enabled", "restrict_user_defined_variables": true, "ci_pipeline_variables_minimum_override_role": "maintainer"}}
	results == set()
}

test_report_override_role_is_developer if {
	results := violation with input as {"project": {"empty_repo": false, "repository_access_level": "enabled", "restrict_user_defined_variables": true, "ci_pipeline_variables_minimum_override_role": "developer"}}

	results == {{
		"msg": "Restrict who can override variables",
		"description": "Limit the ability to override variables to only users with at least the maintainer role\n\nsee https://docs.gitlab.com/ee/ci/variables/#restrict-pipeline-variables",
		"key": "restrict_user_defined_variables",
	}}
}

test_report_restrict_user_defined_variables_disabled if {
	results := violation with input as {"project": {"empty_repo": false, "repository_access_level": "enabled", "restrict_user_defined_variables": false}}

	results == {{
		"msg": "Restrict who can override variables",
		"description": "Limit the ability to override variables to only users with at least the maintainer role\n\nsee https://docs.gitlab.com/ee/ci/variables/#restrict-pipeline-variables",
		"key": "restrict_user_defined_variables",
	}}
}

test_dont_report_for_empty_repos if {
	results := violation with input as {"project": {"empty_repo": true}}
	results == set()
}

test_dont_report_for_disabled_repos if {
	results := violation with input as {"project": {"repository_access_level": "disabled"}}
	results == set()
}
