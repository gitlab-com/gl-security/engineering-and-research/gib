package gitlab.projects.pre_receive_secret_detection

import rego.v1

test_dont_report_pre_receive_secret_detection_enabled if {
	results := violation with input as {"project": {"empty_repo": false, "repository_access_level": "enabled", "pre_receive_secret_detection_enabled": true}}
	results == set()
}

test_report_pre_receive_secret_detection_disabled if {
	results := violation with input as {"project": {"empty_repo": false, "repository_access_level": "enabled", "pre_receive_secret_detection_enabled": false}}
	results == {{
		"msg": "Secret Push Protection is not enabled",
		"description": "Secret Push Protection mush be enabled for this project.\n\nSee https://docs.gitlab.com/ee/user/application_security/secret_detection/secret_push_protection/",
		"key": "spp_not_enabled",
	}}
}

test_dont_report_for_empty_repos if {
	results := violation with input as {"project": {"empty_repo": true}}
	results == set()
}

test_dont_report_for_disabled_repos if {
	results := violation with input as {"project": {"repository_access_level": "disabled"}}
	results == set()
}
