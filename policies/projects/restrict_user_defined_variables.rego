package gitlab.projects.restrict_user_defined_variables

import rego.v1

import data.gitlab.projects.lib
import input.project

violation contains {"description": "Limit the ability to override variables to only users with at least the maintainer role\n\nsee https://docs.gitlab.com/ee/ci/variables/#restrict-pipeline-variables", "key": "restrict_user_defined_variables", "msg": "Restrict who can override variables"} if {
	lib.repository_is_active
	developers_can_override_variables
}

developers_can_override_variables if {
	not project.restrict_user_defined_variables == true
}

developers_can_override_variables if {
	project.restrict_user_defined_variables == true

	# anything else than "developer" is ok ("maintainer", "owner", "no_one_allowed")
	project.ci_pipeline_variables_minimum_override_role == "developer"
}
