package gitlab.projects.tidy

import rego.v1

test_marked_for_deletion if {
	# marked_for_deletion has priority over deprecated
	results := violation with input as {"categories": ["marked_for_deletion", "deprecated"]}
	results == {{"msg": "Must be deleted", "description": "Project marked for deletion", "key": "marked_for_deletion"}}
}

test_marked_for_archiving if {
	results := violation with input as {"categories": ["deprecated"]}
	results == {{"msg": "Must be archived", "description": "Project is deprecated and must be archived", "key": "marked_for_archiving"}}
}
