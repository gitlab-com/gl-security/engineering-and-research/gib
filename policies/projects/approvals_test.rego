package gitlab.projects.approvals

import rego.v1

# Authors can approve their own MR tests

test_author_can_approve_own_MR if {
	results := violation with input as {
		"categories": ["product"],
		"approvals": {"merge_requests_author_approval": true},
	}
	results == {{
		"msg": "Merge Request author can self-approve",
		"description": "Author should not be able to approve own MR",
		"key": "author_can_approve",
	}}
}

test_author_cannot_approve_own_MR if {
	count(violation) == 0 with input as {
		"categories": ["product"],
		"approvals": {"merge_requests_author_approval": false},
	}
}

# Approval rules can change on MRs test

test_approval_rules_can_change_on_individual_MR if {
	results := violation with input as {
		"categories": ["product"],
		"approvals": {"disable_overriding_approvers_per_merge_request": false},
	}
	results == {{
		"msg": "Users can edit MR Approval Rules",
		"description": "Users should not be able to edit approval rules on individual MRs",
		"key": "users_can_edit_approval_rules",
	}}
}

test_approval_rules_cannot_change_on_individual_MR if {
	count(violation) == 0 with input as {
		"categories": ["product"],
		"approvals": {"disable_overriding_approvers_per_merge_request": true},
	}
}

# Committers can approve after own commits test

test_committers_can_approve_own_commits if {
	results := violation with input as {
		"categories": ["product"],
		"approvals": {"merge_requests_disable_committers_approval": false},
	}
	results == {{
		"msg": "Merge Request committers can self-approve",
		"description": "Committers should not be able to approve an MR after adding a commit",
		"key": "committers_can_approve",
	}}
}

test_committers_cannot_approve_own_commits if {
	count(violation) == 0 with input as {
		"categories": ["product"],
		"approvals": {"merge_requests_disable_committers_approval": true},
	}
}

# All approvals are not removed when commits are added

test_all_approvals_are_not_removed_when_commits_added if {
	results := violation with input as {
		"categories": ["product"],
		"approvals": {
			"selective_code_owner_removals": false,
			"reset_approvals_on_push": false,
		},
	}
	results == {{
		"msg": "Approvals are not removed when new commits are added",
		"description": "When new commits are added, approvals should be removed",
		"key": "approvals_not_removed",
	}}
}

test_all_approvals_are_not_removed_when_commits_added_and_codeowners_not_available if {
	results := violation with input as {
		"categories": ["product"],
		"approvals": {"reset_approvals_on_push": false}, # When selective_code_owner_removals is missing from API result only this value is returned
	}
	results == {{
		"msg": "Approvals are not removed when new commits are added",
		"description": "When new commits are added, approvals should be removed",
		"key": "approvals_not_removed",
	}}
}

test_all_approvals_are_removed_when_commits_added if {
	count(violation) == 0 with input as {
		"categories": ["product"],
		"approvals": {
			"selective_code_owner_removals": false,
			"reset_approvals_on_push": true,
		},
	}
}

# CODEOWNER approvals are not removed when commits are added

test_codeowner_approvals_are_not_removed_when_commits_added if {
	project_input := {
		"categories": ["product"],
		"approvals": {
			"selective_code_owner_removals": true,
			"reset_approvals_on_push": false,
		},
		"project": {"default_branch": "main"},
		"protected_branches": [{
			"id": 39065558, "name": "main",
			"code_owner_approval_required": false,
		}],
	}
	results := violation with input as project_input
	results == {{
		"msg": "CODEOWNER approvals are not removed when new commits to their files are added",
		"description": "When new commits are added to CODEOWNER controlled files, approvals should be removed",
		"key": "codeowner_approvals_not_removed",
	}}
}

test_codeowner_approvals_are_not_removed_when_commits_added if {
	project_input := {
		"categories": ["product"],
		"approvals": {
			"selective_code_owner_removals": true,
			"reset_approvals_on_push": false,
		},
		"project": {"default_branch": "main"},
		"protected_branches": [{
			"id": 39065558, "name": "main",
			"code_owner_approval_required": false,
		}],
	}
	results := violation with input as project_input
	results == {{
		"msg": "CODEOWNER approvals are not removed when new commits to their files are added",
		"description": "When new commits are added to CODEOWNER controlled files, approvals should be removed",
		"key": "codeowner_approvals_not_removed",
	}}
}

test_codeowner_approvals_are_removed_when_commits_added if {
	count(violation) == 0 with input as {
		"categories": ["product"],
		"approvals": {
			"selective_code_owner_removals": true,
			"reset_approvals_on_push": false,
		},
		"project": {"default_branch": "main"},
		"protected_branches": [{
			"id": 39065558, "name": "main",
			"code_owner_approval_required": true,
		}],
	}
}
