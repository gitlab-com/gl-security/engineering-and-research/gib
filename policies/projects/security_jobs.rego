package gitlab.projects.security_jobs

import rego.v1

import data.gitlab.projects.lib
import input.categories
import input["ci-config"].merged_yaml as ci

reports contains name if {
	some i
	ci[i].artifacts.reports[name]

	# Don't match hidden jobs
	# See https://gitlab.com/gitlab-com/gl-security/product-security/gib/-/issues/70
	not startswith(i, ".")
}

sast_configured if {
	reports.sast
}

# Check if Advanced SAST is enabled
advanced_sast_enabled if {
	ci.variables.GITLAB_ADVANCED_SAST_ENABLED == "true"
}

# Container Scanning defines 2 reports: one for Dependency Scanning, one for Container Scanning
# If we only test the presence of the Dependency Scanning one, we could actually miss Dependency Scanning
dependency_scanning_configured if {
	some i
	ci[i].artifacts.reports.dependency_scanning
	not ci[i].artifacts.reports.container_scanning

	# Don't match hidden jobs
	# See https://gitlab.com/gitlab-com/gl-security/product-security/gib/-/issues/70
	not startswith(i, ".")
}

container_scanning_configured if {
	reports.container_scanning
}

dast_configured if {
	reports.dast
}

secret_detection_configured if {
	reports.secret_detection
}

sast[{"description": description, "key": key, "msg": msg}] if {
	key = "sast_not_configured"
	not sast_configured
	msg := "SAST is not configured"
	description := "SAST is not configured"
}

sast[{"description": description, "key": key, "msg": msg}] if {
	key = "sast_is_disabled"
	v := ci.variables.SAST_DISABLED
	v != "false"
	msg := "SAST_DISABLED is not 'false'"
	description := sprintf("SAST_DISABLED is set to %q", [v])
}

sast[{"description": description, "key": key, "msg": msg}] if {
	key = "advanced_sast_not_enabled"
	sast_configured
	not advanced_sast_enabled
	msg := "Advanced SAST is not enabled"
	description := "GITLAB_ADVANCED_SAST_ENABLED is not set to 'true'"
}

dependency_scanning[{"description": description, "key": key, "msg": msg}] if {
	key = "dependency_scanning_not_configured"
	not dependency_scanning_configured
	msg := "Dependency Scanning is not configured"
	description := "Dependency Scanning is not configured"
}

dependency_scanning[{"description": description, "key": key, "msg": msg}] if {
	key = "dependency_scanning_is_disabled"
	v := ci.variables.DEPENDENCY_SCANNING_DISABLED
	v != "false"
	msg := "DEPENDENCY_SCANNING_DISABLED is not 'false'"
	description := sprintf("DEPENDENCY_SCANNING_DISABLED is set to %q", [v])
}

secret_detection[{"description": description, "key": key, "msg": msg}] if {
	key = "secret_detection_not_configured"
	not secret_detection_configured
	msg := "Secret Detection is not configured"
	description := "Secret Detection is not configured"
}

secret_detection[{"description": description, "key": key, "msg": msg}] if {
	key = "secret_detection_is_disabled"
	v := ci.variables.SECRET_DETECTION_DISABLED
	v != "false"
	msg := "SECRET_DETECTION_DISABLED is not 'false'"
	description := sprintf("SECRET_DETECTION_DISABLED is set to %q", [v])
}

container_scanning[{"description": description, "key": key, "msg": msg}] if {
	key = "container_scanning_not_configured"
	not container_scanning_configured
	msg := "Container Scanning is not configured"
	description := "Container Scanning is not configured"
}

container_scanning[{"description": description, "key": key, "msg": msg}] if {
	key = "container_scanning_is_disabled"
	v := ci.variables.CONTAINER_SCANNING_DISABLED
	v != "false"
	msg := "CONTAINER_SCANNING_DISABLED is not 'false'"
	description := sprintf("CONTAINER_SCANNING_DISABLED is set to %q", [v])
}

# Don't scan the whole repo history every time
secret_detection_historic_scan[{"description": description, "key": key, "msg": msg}] if {
	v := secrets_historic_scan_is_enabled
	v != "false"
	key = "secrets_historic_scan_is_enabled"
	msg := "SECRET_DETECTION_HISTORIC_SCAN is not 'false'"
	description := sprintf("SECRET_DETECTION_HISTORIC_SCAN is set to %q", [v])
}

secrets_historic_scan_is_enabled if {
	ci.variables[_].SECRET_DETECTION_HISTORIC_SCAN
}

secrets_historic_scan_is_enabled if {
	ci[_].variables[_].SECRET_DETECTION_HISTORIC_SCAN
}

# `red-data`, `product`, `library` | [SAST], [Dependency Scanning] and [Secret Detection] must be enabled |
violation contains {"description": description, "key": key, "msg": msg} if {
	lib.critical_project
	sast[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	lib.critical_project
	dependency_scanning[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	lib.critical_project
	secret_detection[{"msg": msg, "description": description, "key": key}]
}

# `product` + `container` | [Container Scanning] must be enabled |
violation contains {"description": description, "key": key, "msg": msg} if {
	categories[_] == "product"
	categories[_] == "container"
	container_scanning[{"msg": msg, "description": description, "key": key}]
}

# `use_pat`, `website`+`external` | [Dependency Scanning] and [Secret Detection] must be enabled |
violation contains {"description": description, "key": key, "msg": msg} if {
	categories[_] == "use_pat"
	dependency_scanning[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	categories[_] == "use_pat"
	secret_detection[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	categories[_] == "website"
	categories[_] == "external"
	dependency_scanning[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	categories[_] == "website"
	categories[_] == "external"
	secret_detection[{"msg": msg, "description": description, "key": key}]
}

violation contains {"description": description, "key": key, "msg": msg} if {
	secret_detection_historic_scan[{"msg": msg, "description": description, "key": key}]
}

# `docs` | [Secret Detection] must be enabled |
violation contains {"description": description, "key": key, "msg": msg} if {
	categories[_] == "docs"
	secret_detection[{"msg": msg, "description": description, "key": key}]
}
