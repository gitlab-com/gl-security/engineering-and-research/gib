package gitlab.projects.protected_branches

import rego.v1

test_default_branch_is_protected if {
	project_input := {
		"categories": ["product"],
		"project": {"default_branch": "main"},
		"protected_branches": [{
			"id": 39065558, "name": "main",
			"push_access_levels": [{"access_level": 0, "access_level_description": "No one", "user_id": 0, "group_id": 0}],
			"merge_access_levels": [{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null}],
			"allow_force_push": false,
			"unprotect_access_levels": [],
			"code_owner_approval_required": true,
		}],
	}
	results := violation with input as project_input
	results == set()
}

# test examples from https://docs.gitlab.com/ee/user/project/protected_branches.html
test_default_branch_is_protected_by_wildcard if {
	project_input := {
		"project": {"default_branch": "production-stable"},
		"protected_branches": [{"name": "*-stable"}],
	}
	default_branch_is_protected with input as project_input
}

test_default_branch_is_protected_by_wildcard_with_slash if {
	project_input := {
		"project": {"default_branch": "production/app-server"},
		"protected_branches": [{"name": "production/*"}],
	}
	default_branch_is_protected with input as project_input
}

test_default_branch_is_protected_by_wildcard_with_double_wildcard if {
	project_input := {
		"project": {"default_branch": "master/gitlab/production"},
		"protected_branches": [{"name": "*gitlab*"}],
	}
	default_branch_is_protected with input as project_input
}

test_default_branch_with_dots_is_protected_by_wildcard if {
	project_input := {
		"project": {"default_branch": "v6-14.1-security-checks"},
		"protected_branches": [{"name": "*-security-checks"}],
	}
	default_branch_is_protected with input as project_input
}

test_default_branch_is_not_protected if {
	project_input := {
		"categories": ["product"],
		"project": {"default_branch": "main"},
		"protected_branches": [],
	}

	results := violation with input as project_input
	results == {{"msg": "Default branch not protected", "description": "Default branch not protected", "key": "default_branch_not_protected"}}
}

test_default_branch_is_not_protected_but_repo_is_empty if {
	project_input := {
		"categories": ["product"],
		"project": {"default_branch": "main", "empty_repo": true, "repository_access_level": "enabled"},
		"protected_branches": [],
	}

	results := violation with input as project_input
	results == set()
}

test_default_branch_is_not_protected_and_repo_is_not_empty_but_repository_access_level_is_disabled if {
	project_input := {
		"categories": ["product"],
		"project": {"default_branch": "main", "empty_repo": false, "repository_access_level": "disabled"},
		"protected_branches": [],
	}

	results := violation with input as project_input
	results == set()
}

test_default_branch_allows_maintainers_to_push if ``

test_default_branch_allows_maintainers_to_push if {
	project_input := {
		"categories": ["product"],
		"project": {"default_branch": "main"},
		"protected_branches": [{
			"id": 39065558, "name": "main",
			"push_access_levels": [
				{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null},
				{"access_level": 40, "access_level_description": "SomeBot", "user_id": null, "group_id": null},
			],
			"merge_access_levels": [{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null}],
			"allow_force_push": false,
			"unprotect_access_levels": [],
			"code_owner_approval_required": true,
		}],
	}
	results := violation with input as project_input
	results == {{"msg": "Users can push", "description": "Users can push to default branch: [\"Maintainers\", \"SomeBot\"]", "key": "users_can_push"}}
}

test_default_branch_allows_maintainers_to_push_but_repo_is_empty if {
	project_input := {
		"categories": ["product"],
		"project": {"default_branch": "main", "empty_repo": true, "repository_access_level": "enabled"},
		"protected_branches": [{
			"id": 39065558, "name": "main",
			"push_access_levels": [{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null}],
			"merge_access_levels": [{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null}],
			"allow_force_push": false,
			"unprotect_access_levels": [],
			"code_owner_approval_required": true,
		}],
	}
	results := violation with input as project_input
	results == set()
}

test_default_branch_allows_developers_to_merge if {
	project_input := {
		"categories": ["product"],
		"project": {"default_branch": "main"},
		"protected_branches": [{
			"id": 39065558, "name": "main",
			"push_access_levels": [],
			"merge_access_levels": [
				{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null},
				{"access_level": 30, "access_level_description": "Developers + Maintainers", "user_id": null, "group_id": null},
			],
			"allow_force_push": false,
			"unprotect_access_levels": [],
			"code_owner_approval_required": true,
		}],
	}
	results := violation with input as project_input
	results == {{"msg": "Developers can merge", "description": "Developers can merge to default branch", "key": "developers_can_merge"}}
}

test_default_branch_allows_developers_to_merge_but_repo_is_empty if {
	project_input := {
		"categories": ["product"],
		"project": {"default_branch": "main", "empty_repo": true, "repository_access_level": "enabled"},
		"protected_branches": [{
			"id": 39065558, "name": "main",
			"push_access_levels": [],
			"merge_access_levels": [
				{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null},
				{"access_level": 30, "access_level_description": "Developers + Maintainers", "user_id": null, "group_id": null},
			],
			"allow_force_push": false,
			"unprotect_access_levels": [],
			"code_owner_approval_required": true,
		}],
	}
	results := violation with input as project_input
	results == set()
}

test_default_branch_allows_some_bot_to_push if {
	# SomeBot is allowed to push to the default branch
	project_input := {
		"categories": ["product"],
		"protected_branches_allow_list": [{
			"name": "main",
			"push_access_levels": [
				{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null},
				{"access_level": 40, "access_level_description": "SomeBot", "user_id": 12345, "group_id": null},
			],
		}],
		"project": {"default_branch": "main", "empty_repo": false},
		"protected_branches": [{
			"id": 39065558, "name": "main",
			"push_access_levels": [
				{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null},
				{"access_level": 40, "access_level_description": "SomeBot", "user_id": 12345, "group_id": null},
			],
			"merge_access_levels": [],
			"allow_force_push": false,
			"unprotect_access_levels": [],
			"code_owner_approval_required": true,
			"allow_list_protected_branches": [],
		}],
	}
	results := violation with input as project_input
	results == set()
}

test_default_branch_unallowed_user_can_push if {
	# SomeBot is allowed to push to the default branch
	project_input := {
		"categories": ["product"],
		"protected_branches_allow_list": [{
			"name": "main",
			"push_access_levels": [{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null}],
		}],
		"project": {"default_branch": "main", "empty_repo": false},
		"protected_branches": [{
			"id": 39065558, "name": "main",
			"push_access_levels": [
				{"id": 39065559, "access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null},
				{"id": 39065560, "access_level": 40, "access_level_description": "SomeBot", "user_id": 12345, "group_id": null},
			],
			"merge_access_levels": [],
			"allow_force_push": false,
			"unprotect_access_levels": [],
			"code_owner_approval_required": true,
			"allow_list_protected_branches": [],
		}],
	}
	results := violation with input as project_input
	results == {{"msg": "Users can push", "description": "Users can push to default branch: [\"Maintainers\", \"SomeBot\"] (allowed: [\"Maintainers\"])", "key": "users_can_push"}}
}

test_another_branch_allows_developers_to_push if {
	project_input := {
		"categories": ["product"],
		"project": {"default_branch": "main"},
		"protected_branches": [
			{
				"id": 39065558, "name": "main",
				"push_access_levels": [],
				"merge_access_levels": [{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null}],
				"allow_force_push": false,
				"unprotect_access_levels": [],
				"code_owner_approval_required": true,
			},
			{
				"id": 39065558, "name": "another_branch",
				"push_access_levels": [
					{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null},
					{"access_level": 30, "access_level_description": "Developers + Maintainers", "user_id": null, "group_id": null},
				],
				"merge_access_levels": [{"access_level": 40, "access_level_description": "Maintainers", "user_id": null, "group_id": null}],
				"allow_force_push": false,
				"unprotect_access_levels": [],
				"code_owner_approval_required": true,
			},
		],
	}
	results := violation with input as project_input
	results == set()
}
