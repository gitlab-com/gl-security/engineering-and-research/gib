#!/bin/bash
#
# This script can be used to generate a new inventory site from the inventory_test folder
#

set -e

if [ "$(docker images -q gib 2>/dev/null)" == "" ]; then
  docker build -t gib --build-arg ARCH=arm64 .
fi
rm -rf ./inventory_test/tmp/inventory
docker run -it --rm -v "$PWD/inventory_test:/inventory" -w /inventory gib compliance run
docker run -it --rm -v "$PWD/inventory_test:/inventory" -w /inventory gib update-db
docker run -it --rm -v "$PWD/inventory_test:/inventory" -w /inventory gib compliance export
docker run -it --rm -v "$PWD/inventory_test:/inventory" -w /inventory -e GITLAB_API_TOKEN=test gib generate-reports

if [ -z "$GITLAB_API_TOKEN" ]; then
  echo "Warning: GITLAB_API_TOKEN is not set, some content might not be fetched during site generation."
fi
docker run -it --rm \
  -v "$PWD/inventory_test:/inventory" \
  -w /inventory/tmp/inventory \
  -e GITLAB_API_TOKEN="${GITLAB_API_TOKEN:-test}" \
  -e HUGO_BASEURL=http://localhost:1313/inventory/ \
  --entrypoint=hugo -p 1313:1313 \
  -v "$PWD/reports/theme/:/inventory/tmp/inventory/themes/gitlab" \
  --name inventory_dev \
  gib serve --logLevel debug --bind="0.0.0.0" --poll 500ms
open http://localhost:1313
