# Groups Fixtures

## projects.json

list of projects under the `gitlab-org` group.

#### update

```sh
curl https://gitlab.com/api/v4/groups/gitlab-org/projects | json_pp > projects.json
```

## subgroups.json

list of groups under the `gitlab-org` group.

#### update

```sh
curl https://gitlab.com/api/v4/groups/gitlab-org/subgroups?per_page=4 | json_pp > subgroups.json
```

## 5-minute-production-app_projects.json

```sh
curl https://gitlab.com/api/v4/groups/gitlab-org%2F5-minute-production-app/projects\?per_page\=2 | json_pp > 5-minute-production-app_projects.json
```

## archived.json

This file is private, and must be retrieved with a Personal Access Token. The project is empty, and doesn't contain any confidential data.

```sh
curl --header "PRIVATE-TOKEN: <your_access_token>" https://gitlab.com/api/v4/projects/gitlab-org%2fgitlab-developement-kit | json_pp > 5-minute-production-app_projects.json
```
